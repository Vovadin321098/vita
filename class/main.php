<?php
function render($template,$render){
    include('twig/autoloader.php');
    Twig_Autoloader::register();
    $loader= new Twig_Loader_Filesystem('templates');
    $twig= new Twig_Environment($loader);
    $template= $twig->loadTemplate($template);
    echo $template->render($render);


}
?>